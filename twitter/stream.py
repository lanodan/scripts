#!/usr/bin/python3
# -*- encoding: utf-8 -*-
import html, sys
import config

#o = TwitterOAuth.read_file('credentials.txt')
#api = TwitterAPI(
#	o.consumer_key,
#	o.consumer_secret,
#	o.access_token_key,
#	o.access_token_secret)

def main(user):
	import common
	api = common.api(config.credentials['consumer_key'], config.credentials['consumer_secret'], user['access_token_key'], user['access_token_secret'])
	
	r = api.request('user', {'replies': 'all'})
	for item in r:
		if 'text' in item:
			print(common.f['tweet'] % {'id': item['id'], 'screen_name': item['user']['screen_name'], 'name': item['user']['name'], 'text': html.unescape(item['text'])})
		elif 'friends' in item:
			print(common.friend_list_garbage)
		elif 'event' in item:
			try:
				if item['event'] in ('favorite', 'favorited_retweet'):
					print(common.f['favorite'] % { item['source']['screen_name'], item['event'], item['target_object']['user']['screen_name'], item['target_object']['text']})
				elif item['event'] in ('retweet', 'retweeted_retweet'):
					print(common.f['retweet'] % { item['source']['screen_name'], item['event'], item['target_object']['user']['screen_name'], item['target_object']['text']})
				elif item['event'] == 'follow':
					print(common.f['follow'] % { item['source']['screen_name'], item['event'], item['target']['user']['screen_name']})
				else:
					print(common.f['default'] % { item['source']['screen_name'], item['event'], item['target_object']['user']['screen_name'], item['target_object']['text']})
			except:
				print(item)
				print(sys.exc_info())
		else:
			print(item)

user = dict()
username = list(config.credentials['users'])[0]
#if __name__ == "__main__":
#	try:
#		username = sys.argv[0]
#	except KeyError:
#		print('Protip: you can specify an user account, anyway using first account')
#		username = list(config.credentials['users'])[0]

user = config.credentials['users'][username]
user['name'] = username
username = None
main(user)
