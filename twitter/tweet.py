#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager
import sys

o = TwitterOAuth.read_file('credentials.txt')
api = TwitterAPI(
	o.consumer_key,
	o.consumer_secret,
	o.access_token_key,
	o.access_token_secret)

def favorite(id):
	""" required argument, id """
	api.request('favorites/create', {'id': id})

def retweet(id):
	""" required argument: id """
	api.request('statuses/retweet/:{}'.format(id))

def reply(in_reply_to_status_id, status):
	""" required arguments: in_reply_to_status_id, status """
	api.request('statuses/update', {'in_reply_to_status_id': in_reply_to_status_id, 'status': status})

def delete(id):
	""" required argument: id """
	api.request('statuses/destroy/:{}'.format(id))

def nsfw(status):
	""" required argument: status """
	api.request('statuses/update', {'status': status, 'possibly_sensitive': True})

def upload(file_path, status):
	""" required argument: file_path, status """
	file = open(file_path, 'rb')
	r = api.request('media/upload', None, {'media': file.read()})
	api.request('statuses/update', {'status': status, 'media_ids': r.json()['media_id']})
	file.close()

def upload_nsfw(file_path, status):
	""" required argument: file_path, status """
	file = open(file_path, 'rb')
	r = api.request('media/upload', None, {'media': file.read()})
	api.request('statuses/update', {'status': status, 'media_ids': r.json()['media_id'], 'possibly_sensitive': True})
	file.close()

def follow(target):
	if target.isalpha():
		api.request('friendships/create', {'screen_name': target})
	else:
		api.request('friendships/create', {'screen_name': target})
	
def unfollow(target):
	if target.isalpha():
		api.request('friendships/destroy', {'screen_name': target})
	else:
		api.request('friendships/destroy', {'user_id': target})
	
def parse_command(status):
	""" Delete: /<del|delete|destroy> <tweet_id> """
	""" Favorite & Retweet: /<frt|favrt> <tweet_id> """
	""" Favorite: /<f|fav|favorite> <tweet_id> """
	""" NSFW: /nsfw <status> """
	""" Reply: /<re|reply> <tweet_id> <status> """
	""" Retweet: /<rt|retweet> <tweet_id> """
	""" Upload: /upload <tweet_id> """
	""" Follow: /follow <screen_name|user_id> """
	""" Unfollow: /unfollow <screen_name|user_id> """
	""" To tweet just put it without any command. Image uploading not supported yet """
	""" Protip: To send a direct message prepend MS @<name> or M$ @<name> """
	if status[0] == '/':
		command = status[1:].split(' ')
		if command[1] is not None:
			if command[0] in ('f', 'fav', 'favorite'):
				favorite(id = command[1])
			elif command[0] in ('rt', 'retweet'):
				retweet(id = command[1])
			elif command[0] in ('frt', 'favrt'):
				favorite(id = command[1])
				retweet(id = command[1])
			elif command[0] in ('re', 'reply'):
				reply(in_reply_to_status_id = command[1], status = status.split(' ', 2)[2])
			elif command[0] in ('del', 'delete', 'destroy'):
				delete(command[1])
			elif command[0] == 'nsfw':
				nsfw(status = command[1])
			elif command[0] == 'upload':
				upload(status = status.split(' ', 2)[2], file_path = command[1])
			elif command[0] == 'upload_nsfw':
				upload(status = status.split(' ', 2)[2], file_path = command[1])
			elif command[0] == 'follow':
				follow(command[1])
			elif command[0] == 'unfollow':
				unfollow(command[1])
			elif command[0] == 'tweet':
				print('No command is used to tweet, just put it without /tweet')
			else:
				print(status, 'doesn’t exist.')
				_help()
		else:
			print(status, 'you’ve forget arguments.')
			_help()
	else:
		if len(status) > 140:
			print('Tweets have a max of 140 charaters')
		elif len(status) == 0:
			print('Tweets cannot be empty')
		else:
			print(api.request('statuses/update', {'status': status}))

if __name__ == "__main__":
	while 1:
		prompt = input('>>')
		parse_command(prompt)
