#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager
import html, sys

o = TwitterOAuth.read_file('credentials.txt')
api = TwitterAPI(
	o.consumer_key,
	o.consumer_secret,
	o.access_token_key,
	o.access_token_secret)
