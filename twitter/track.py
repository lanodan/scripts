#!/usr/bin/python3
# -*- encoding: utf-8 -*-
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager
import sys

o = TwitterOAuth.read_file('credentials.txt')
api = TwitterAPI(
	o.consumer_key,
	o.consumer_secret,
	o.access_token_key,
	o.access_token_secret)

try:
	r = api.request('statuses/filter', {'track': sys.argv[1:]})
	for item in r:
		print(item['text'] if 'text' in item else item)
except:
	print(sys.exc_info())
