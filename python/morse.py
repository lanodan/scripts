def toMorseArray(s):
    """Return a list of the patterns to use for s."""
    return [lookup[c] for c in s.upper()]

def toMorseString(s):
	out = ' '
	for c in s.upper():
		try:
			out = str(out + lookup[c] + ' ')
		except KeyError:
		    print('[Error] KeyError')
	return out

def fromMorse(s):
	out = ' '
	for c in s.split(' '):
		try:
			out = str(out + inv_lookup[c])
		except KeyError:
			print('[Error] KeyError')
	return out

lookup = {'!': '-.-.--',
          "'": '.----.',
          '"': '.-..-.',
          '$': '...-..-',
          '&': '.-...',
          '(': '-.--.',
          ')': '-.--.-',
          '+': '.-.-.',
          ',': '--..--',
          '-': '-....-',
          '.': '.-.-.-',
          '/': '-..-.',
          '0': '-----',
          '1': '.----',
          '2': '..---',
          '3': '...--',
          '4': '....-',
          '5': '.....',
          '6': '-....',
          '7': '--...',
          '8': '---..',
          '9': '----.',
          ':': '---...',
          ';': '-.-.-.',
          '=': '-...-',
          '?': '..--..',
          '@': '.--.-.',
          'A': '.-',
          'B': '-...',
          'C': '-.-.',
          'D': '-..',
          'E': '.',
          'F': '..-.',
          'G': '--.',
          'H': '....',
          'I': '..',
          'J': '.---',
          'K': '-.-',
          'L': '.-..',
          'M': '--',
          'N': '-.',
          'O': '---',
          'P': '.--.',
          'Q': '--.-',
          'R': '.-.',
          'S': '...',
          'T': '-',
          'U': '..-',
          'V': '...-',
          'W': '.--',
          'X': '-..-',
          'Y': '-.--',
          'Z': '--..',
          '_': '..--.-',
		  ' ': ' ',
          }
inv_lookup = {lookup[k] : k for k in lookup}
