<?php
header("HTTP/1.0 418 I'm a teapot");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>418 I'm a teapot</title>
  </head>
  <body>
    <h1>I'm a teapot</h1>
    <p>The teapot failed to get you some coffee.</p>
    <hr>
    <address><?=$_SERVER["SERVER_SIGNATURE"]?></address>
  </body>
</html>
