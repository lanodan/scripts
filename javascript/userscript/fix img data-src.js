// ==UserScript==
// @name        fix img data-src
// @namespace   https://hacktivis.me/
// @description assign data-src attribute to src attribute on images
// @include     http://knowyourmeme.com/*
// @include     http://mrjakeparker.com/*
// @version     1.0.1
// @grant       none
// ==/UserScript==

var input = document.getElementsByTagName("img");
var src;

for (i = 0; i < input.length; i++) {
	if (src = input[i].getAttribute("data-src")) { input[i].setAttribute("src", src); }
}
