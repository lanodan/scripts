#!/bin/sh
# Copyright 2017 CC-BY-SA-4.0 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
# Script that generates a photo gallery with details on dedicated pages

## Generate the index

test -e index.html || (
	title=$(basename "${PWD}")
	echo "<!DOCTYPE html>
<html>
	<head>
		<meta charset=\"utf-8\"/>
		<title>${title} — AFKs of lanodan</title>
		<link rel=\"stylesheet\" href=\"../style.css\" />
	</head>
	<body>
	<h1>${title}</h1>
	<main>"
for img in *.JPG; do
	test -e "./thumbs/$img" || gm convert "$img" -resize 8% -auto-orient -quality 69% "./thumbs/$img"
	echo "<a class=\"photo\" href=\"${img//.JPG/.html}\"><img src=\"thumbs/$img\"/></a>";
done
echo "	</main>
	<footer rel=\"copyright\">© 2014-2017 <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">CC-BY-SA-4.0</a> Haelwenn (lanodan) Monnier</footer>
	</body>
</html>" ) | tee index.html

## Generate pages for images (with EXIF data)

for img in *.JPG; do
	test -e "${img//.JPG/.html}" || (
		echo "<!DOCTYPE html>
<html>
	<head>
		<meta charset=\"utf-8\"/>
		<title>$(basename "${PWD}")/${img//.JPG} — AFKs of lanodan</title>
		<link rel=\"stylesheet\" href=\"../style.css\" />
	</head>
	<body>
	<h1><a href=\"./\">$(basename "${PWD}")</a>/${img//.JPG}</h1>
	<main>"
test -e "./preview/$img" || gm convert "$img" -resize 50% -auto-orient -quality 80% "./preview/$img"
echo "<a class=\"photo\" href=\"$img\"><img src=\"preview/$img\"/></a>"
gm identify "$img" -format '<ul>
	<li>Camera: %[EXIF:Make]%[EXIF:Model]'
printf '	<li>DateTime: '; date "+%F %T%z" --date="$(gm identify "$img" -format '%[EXIF:DateTime]'|sed -r 's/:(..):/-\1-/')Z"
gm identify "$img" -format '	<li>ExposureTime: %[EXIF:ExposureTime]
	<li>ISOSpeedRatings: %[EXIF:ISOSpeedRatings]
	<li>FNumber: %[EXIF:FNumber]
	<li>MaxApertureValue: %[EXIF:MaxApertureValue]
	<li rel=\"copyright\">© 2014-2017 <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">CC-BY-SA-4.0</a> Haelwenn (lanodan) Monnier</li>
</ul>'
echo "	</main>
	</body>
</html>") | tee "${img//.JPG/.html}"
done
