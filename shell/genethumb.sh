#!/bin/sh
# genethumb.sh - G�n�ration de thumbnails mortels de ta m�re
# (c)  3 Jan 1998: version 0.0.4 by Samuel Hocevar <sam@via.ecp.fr>
#     22 May 2000: version 0.0.5 by Samuel Hocevar <sam@via.ecp.fr>
#     15 Nov 2000: version 0.0.6 by Samuel Hocevar <sam@zoy.org>
#                    with code from Sven Hartge <hartge@ds9.argh.org>
#     13 Dec 2000: version 0.0.7 by Samuel Hocevar <sam@zoy.org>
#                    now compatible with old ImageMagick versions
#                    added --rows option
#     27 Apr 2003: version 0.0.8 by Sam Hocevar <sam@zoy.org>
#                    removed all bashisms
#                    now compatible with all ImageMagick versions
#     2014-02-28: version 0.0.9 by Haelwenn Monnier (lanodan) <haelwenn.monnier@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

version=0.0.9
THFILE=index.html
THDIR=.thumbnails
THEXT=png      # change this to jpeg or whatever you want
THWIDTH=120    # thumbnail width
THHEIGHT=90    # thumbnail height

if [ "$1" != "" ] then
  THWIDTH=$1
fi

if [ "$2" != "" ] then
  THHEIGHT=$2
fi

echo "genethumb.sh v$version -- report bugs to Haelwenn Monnier (lanodan) <haelwenn.monnier@free.fr>"
echo "usage: genethumb.sh [thumbnails width] [thumbnails height]"

if [ -w $THFILE ]
then
  echo "moving $THFILE to $THFILE~"
  mv -f $THFILE $THFILE~
fi

if [ ! -d $THDIR ]
then
  echo "creating thumbnail directory $THDIR"
  mkdir $THDIR
fi

cat > $THFILE << EOF
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" charset="utf-8"/> <!-- ugly hack to have legacy and html5 encoding -->
		<meta name="GENERATOR" content="genethumb.sh version $version" />
		<meta name="Description" content="thumbnails of $PWD" />
		<title>Index of $PWD</title>
		<style type="text/css">
			a { display:block; }
			td.center { text-align: center; }
			td.nowrap { white-space: nowrap; }
			blockquote.center { text-align: center; }
		</style>
	</head>
	<body>
		<h2>Index of $PWD</h2>
		<span align="right">generated by <a href="http://sam.zoy.org/projects/unix/genethumb.html">genethumb.sh</a> version $version </span>
		<hr />
EOF

#find . -type f -maxdepth 1 | sed 's,^\./,,' | while read file // Uwwh What a ugly command
for file in *.(jpg|jpeg|png|gif); do newfile="$THDIR/$file.$THEXT";
	failed=0
	echo -n "* $file: "

	if [ -r "$newfile" ] then
		echo 'thumbnail already exists.'
	else
		convert -auto-orient -thumbnail ${THWIDTH}x${THHEIGHT} "$file" "$newfile" >/dev/null 2>&1
		if [ ! -r "$newfile" ] then
			echo "failed creating."
			failed=1
		else
			echo "done."
		fi
	fi

	if [ "$failed" = "0" ] then
		if [ -e "$newfile.info" ] then
			read filesize coordinates oldcoords compat < "$newfile.info"
		fi

		if expr "$version" ">" "$compat" >/dev/null 2>&1 then
			read oldcoords filesize << EOF
			`identify -format '%wx%h %b' "$file" | head -1`
EOF
			coordinates=`identify -format '%wx%h' "$newfile" | head -1`
			rm -f "$newfile.info"
			echo $filesize $coordinates $oldcoords $version > "$newfile.info"
			echo "  $newfile.info written."
		fi

		j="`echo $file | cut -b1-20`"
		if [ "$file" != "$j" ] then
			j="`echo $file | cut -b1-17`..."
		fi

		cat >> $THFILE << EOF
		<a href="$file"
			width="`echo $coordinates | cut -f1 -dx`"
			height="`echo $coordinates | cut -f2 -dx`">
			<img alt="$file ($oldcoords)" src="$newfile" /><br />
			$j<br />
			$oldcoords ($filesize)
		</a>
EOF

done
echo "done."

echo '	</body>
</html>' >> $THFILE
