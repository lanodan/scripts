## Python scripts
### image.py
![screenshot](screenshots/image.png "image.py")
### colorpicker.py (GTK3 widget)
![screenshot](screenshots/colorpicker.png "colorpicker.py")

## Photo Gallery
Static (HTML5/CSS3) generator of Photo gallery with detailed information on dedicated pages.
* ``script.sh``: generator (POSIX Shell, gives HTML5)
* ``style.css``: provided/example of CSS styling
* Use case/example: [2014-04-26 → 2014-05-12 Japan — AFKs of lanodan](https://hacktivis.me/photos/2014-04-26%20%E2%86%92%202014-05-12%20Japan/)

### Dependencies
* POSIX Shell
* GraphicsMagick (note: ImageMagick is probably useable by replacing ``gm convert`` with ``convert`` and ``gm identify`` with ``identify``)
